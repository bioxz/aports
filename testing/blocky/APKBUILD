# Contributor: Justin Berthault <justin.berthault@zaclys.net>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=blocky
pkgver=0.23
pkgrel=0
pkgdesc="DNS proxy as ad-blocker for local network"
url="https://github.com/0xERR0R/blocky"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="$pkgname-openrc"
install="$pkgname-openrc.pre-install $pkgname-openrc.pre-upgrade"
source="$pkgname-$pkgver.tar.gz::https://github.com/0xERR0R/blocky/archive/v$pkgver.tar.gz
	busybox-date.patch
	blocky.initd
	blocky.confd
	config.yml
	"
# fail with new go for some reason, even with tzdata/goroot
options="!check net"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	make VERSION=$pkgver build
}

check() {
	make test
}

package() {
	install -Dm755 bin/blocky -t "$pkgdir"/usr/bin/

	# blocky will not start if its log directory is missing
	mkdir -p "$pkgdir"/var/log/blocky
}

openrc() {
	install -Dm755 "$srcdir"/blocky.initd "$subpkgdir"/etc/init.d/blocky
	install -Dm644 "$srcdir"/blocky.confd "$subpkgdir"/etc/conf.d/blocky

	# Provide a simple configuration file from
	# https://0xerr0r.github.io/blocky/v0.23/configuration/
	install -Dm644 "$srcdir"/config.yml "$subpkgdir"/etc/blocky/config.yml
}

sha512sums="
22431da4c2f259411b7eb790e8f05a7264d0aa96fe4774722bbe71cf9459a3ed7f434b4b295c95b80f2efc6de55ff645f860443838fe793bc413d74b77e395be  blocky-0.23.tar.gz
2f1e60037229ad2730f3d51a16e79f0ef93baf80fa73948d08d1216de5db454f10ea4081558f1c86db2e394948cfce62af20b802278dd89241b591e77bce8b4c  busybox-date.patch
01c8a400ae53e7ce4159a5244d90d1f2abe76e59a8b5ffd7f85b7c9c155c9c1750011e44ed39d373804bf00c84405a9e39229fc26d680b5a5650c4cd86d58a42  blocky.initd
5f69299179747fafa3b790cc6a47ffbf916a9befe2e4cedc32b7e55875584d96f98207288a1a8b714031ad59aa356550d5ef292f1d9ef32aa99a20fc55331d0d  blocky.confd
9ede8a4b26aae73e947221f59edc94062c0020d58ef53678d479ed09aef7ab4636a48bdf921eb0c822e72b179885315f227cdc3a32767d147908828f06804692  config.yml
"
